package com.example.kennbican.teamandroidweatherapp;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class WeatherData
{
    String zipCode;
    JsonElement jse;
    JsonArray jsaForecast;
    String satelliteImageURL;
    int sunriseHour;
    int sunriseMinute;
    int sunsetHour;
    int sunsetMinute;

    public WeatherData( String zip )
    {
        zipCode = zip;
    }

    public void fetch()
    {
        String areaCodeConditions = "http://api.wunderground.com/api/" +
                BuildConfig.ApiKey + "/conditions/forecast/hourly/forecast10day/astronomy/q/" + zipCode + ".json";

        satelliteImageURL = "http://api.wunderground.com/api/" +
                BuildConfig.ApiKey + "/radar/satellite/q/" + zipCode + ".png";

        try
        {
            URL wundergroundURL = new URL(areaCodeConditions);

            InputStream is = wundergroundURL.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            jse = new JsonParser().parse(br);

        }
        catch (java.net.MalformedURLException mue)
        {
            System.out.println("URL not well formed");
            mue.printStackTrace();
        }
        catch (java.io.IOException ioe)
        {
            System.out.println("Got IO Exception");
            ioe.printStackTrace();
        }
    }
    public String getSatelliteURL()
    {
        return satelliteImageURL;
    }

    public ArrayList<Forecast> fiveDayForecast()
    {
        if (jse == null) fetch();

        ArrayList<Forecast> forecasts = new ArrayList<>();
        jsaForecast = jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject()
                .get("forecastday").getAsJsonArray();

        for (int i = 0; i < 6; i++)
        {
            String tempDate = jsaForecast.get(i).getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString();
            String tempWeekday = jsaForecast.get(i).getAsJsonObject().get("date").getAsJsonObject().get("weekday").getAsString();
            String tempIcon = jsaForecast.get(i).getAsJsonObject().get("icon").getAsString();
            String tempHighF = jsaForecast.get(i).getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();
            String tempHighC = jsaForecast.get(i).getAsJsonObject().get("high").getAsJsonObject().get("celsius").getAsString();
            String tempLowF = jsaForecast.get(i).getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString();
            String tempLowC = jsaForecast.get(i).getAsJsonObject().get("low").getAsJsonObject().get("celsius").getAsString();
            String tempCondition = jsaForecast.get(i).getAsJsonObject().get("conditions").getAsString();
            String tempPop = jsaForecast.get(i).getAsJsonObject().get("pop").getAsString();

            Forecast tempF= new Forecast(tempDate, tempWeekday, tempIcon, tempHighF, tempHighC,tempLowF, tempLowC,
                    tempCondition, tempPop);

            forecasts.add(tempF);
        }

        return forecasts;
    }

    public Conditions currentConditions()
    {
        if(jse==null)
        {
            fetch();
        }
        Date instant = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat( "HH:mm:ss" );
        String time = sdf.format( instant );

        String WeatherCondition = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("weather").getAsString();
        String location = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();
        String hour = time ;
        String tempF = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsString();
        String tempC = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_c").getAsString();
        String pop = jse.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("pop").getAsString();;
        String windSpeed = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("wind_mph").getAsString();
        String humidity =  jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("relative_humidity").getAsString();
        String windChill = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("feelslike_f").getAsString();
        String windChillC = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("feelslike_c").getAsString();
        String currentTime = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("observation_time").getAsString();
        String icon;

        if (jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon_url").getAsString().contains("nt_"))
        {
            icon = "nt_" + jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon").getAsString();
        }

        else
        {
            icon = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon").getAsString();
        }

        Conditions current = new Conditions (WeatherCondition, icon, location, hour, tempF ,tempC, pop, windSpeed, humidity, windChill, windChillC, currentTime);
        return current;
    }

    public ArrayList<Conditions> fiveHourForecast()
    {
        if(jse==null)
        {
            fetch();
        }
        ArrayList<Conditions> fiveHr;
        fiveHr = new ArrayList();
        JsonArray hourlyArray = jse.getAsJsonObject().get("hourly_forecast").getAsJsonArray();

        for(int i = 0 ; i < 5 ; i++ )
        {
            String weatherCondition = hourlyArray.get(i).getAsJsonObject().get("condition").getAsString();
            String hour = hourlyArray.get(i).getAsJsonObject().get("FCTTIME").getAsJsonObject().get("civil").getAsString();
            String tempF = hourlyArray.get(i).getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsString();
            String tempC = hourlyArray.get(i).getAsJsonObject().get("temp").getAsJsonObject().get("metric").getAsString();
            String pop = hourlyArray.get(i).getAsJsonObject().get("pop").getAsString();
            String icon;

            if (hourlyArray.get(i).getAsJsonObject().get("icon_url").getAsString().contains("nt_"))
            {
                icon = "nt_" + hourlyArray.get(i).getAsJsonObject().get("icon").getAsString();
            }

            else
            {
                icon = hourlyArray.get(i).getAsJsonObject().get("icon").getAsString();
            }

            Conditions temp = new Conditions(weatherCondition, icon, "" , hour, tempF, tempC, pop, "", "", "", "","");
            fiveHr.add(temp);
        }
        return fiveHr;
    }

}