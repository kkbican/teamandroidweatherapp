    package com.example.kennbican.teamandroidweatherapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Image;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebView;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private WeatherData wd;
    private Conditions c;
    private ArrayList<Conditions> fiveHourly;
    private ArrayList<Forecast> fiveForecast;
    private String currentCity = "autoip";

    private TextView currentLocation;
    private TextView currentCondition;
    private TextView currentTemp;
    private ImageView currentIcon;
    private TextView cHigh;
    private TextView cLow;

    private TextView field0;
    private TextView field1;
    private TextView field2;
    private TextView field3;
    private TextView field4;

    private TextView temp0;
    private TextView temp1;
    private TextView temp2;
    private TextView temp3;
    private TextView temp4;

    private TextView tempMin0;
    private TextView tempMin1;
    private TextView tempMin2;
    private TextView tempMin3;
    private TextView tempMin4;

    private String tempQuery;

    private EditText input;

    boolean satView = false;

    private String UIView;

    ImageView icon0;
    ImageView icon1;
    ImageView icon2;
    ImageView icon3;
    ImageView icon4;
    WebView satellite;

    ToggleButton toggleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new GetWeatherInBackground().execute(currentCity);


    }

    private class GetWeatherInBackground extends AsyncTask<String, Void, WeatherData>
    {
        @Override
        protected WeatherData doInBackground(String ... location)
        {
            // fetch weather data
            wd = new WeatherData(location[0]);
            wd.fetch();

            return wd;
        }

        @Override
        protected void onPostExecute(WeatherData wd)
        {
            // update UI
            c = wd.currentConditions();
            fiveHourly = wd.fiveHourForecast();
            fiveForecast = wd.fiveDayForecast();
            toggleButton = findViewById(R.id.toggleButton);
            toggleButton.setText("/C°");
            toggleButton.setTextOff("/C°");
            toggleButton.setTextOn("/F°");

            //current
            currentLocation = findViewById(R.id.currentLocation);
            currentLocation.setText(c.getLocation());
            currentCondition = findViewById(R.id.currentCondition);
            currentCondition.setText(c.getWeatherCondition());
            currentTemp = findViewById(R.id.currentTemp);
            currentIcon = findViewById(R.id.currentIcon);
            String imageString = c.getIcon();
            int resID = getResources().getIdentifier(imageString, "drawable", getPackageName());
            currentIcon.setImageResource(resID);
            cHigh = findViewById(R.id.cHigh);
            cLow = findViewById(R.id.cLow);

            if (toggleButton.isChecked())
            {
                //hourly temp
                temp0 = findViewById(R.id.temp0);
                temp0.setText(fiveHourly.get(0).getTempC());
                temp1 = findViewById(R.id.temp1);
                temp1.setText(fiveHourly.get(1).getTempC());
                temp2 = findViewById(R.id.temp2);
                temp2.setText(fiveHourly.get(2).getTempC());
                temp3 = findViewById(R.id.temp3);
                temp3.setText(fiveHourly.get(3).getTempC());
                temp4 = findViewById(R.id.temp4);
                temp4.setText(fiveHourly.get(4).getTempC());

                //no hourly low
                tempMin0 = findViewById(R.id.tempMin0);
                tempMin0.setText("");
                tempMin1 = findViewById(R.id.tempMin1);
                tempMin1.setText("");
                tempMin2 = findViewById(R.id.tempMin2);
                tempMin2.setText("");
                tempMin3 = findViewById(R.id.tempMin3);
                tempMin3.setText("");
                tempMin4 = findViewById(R.id.tempMin4);
                tempMin4.setText("");

                currentTemp.setText(c.getTempC());
                cHigh.setText("Today: " + fiveForecast.get(0).getHighC());
                cLow.setText(fiveForecast.get(0).getLowC());
                toggleButton.setText("/F°");
            }

            else
            {
                //hourly temp
                temp0 = findViewById(R.id.temp0);
                temp0.setText(fiveHourly.get(0).getTempF());
                temp1 = findViewById(R.id.temp1);
                temp1.setText(fiveHourly.get(1).getTempF());
                temp2 = findViewById(R.id.temp2);
                temp2.setText(fiveHourly.get(2).getTempF());
                temp3 = findViewById(R.id.temp3);
                temp3.setText(fiveHourly.get(3).getTempF());
                temp4 = findViewById(R.id.temp4);
                temp4.setText(fiveHourly.get(4).getTempF());

                //no hourly low
                tempMin0 = findViewById(R.id.tempMin0);
                tempMin0.setText("");
                tempMin1 = findViewById(R.id.tempMin1);
                tempMin1.setText("");
                tempMin2 = findViewById(R.id.tempMin2);
                tempMin2.setText("");
                tempMin3 = findViewById(R.id.tempMin3);
                tempMin3.setText("");
                tempMin4 = findViewById(R.id.tempMin4);
                tempMin4.setText("");

                currentTemp.setText(c.getTempF());
                cHigh.setText("Today: " + fiveForecast.get(0).getHighF());
                cLow.setText(fiveForecast.get(0).getLowF());
            }

            //hourly hour
            field0 = findViewById(R.id.field0);
            field0.setText(fiveHourly.get(0).getHour());
            field1 = findViewById(R.id.field1);
            field1.setText(fiveHourly.get(1).getHour());
            field2 = findViewById(R.id.field2);
            field2.setText(fiveHourly.get(2).getHour());
            field3 = findViewById(R.id.field3);
            field3.setText(fiveHourly.get(3).getHour());
            field4 = findViewById(R.id.field4);
            field4.setText(fiveHourly.get(4).getHour());

            //hourly icons
            icon0 = findViewById(R.id.icon0);
            String iconString0 = fiveHourly.get(0).getIcon();
            int resID0 = getResources().getIdentifier(iconString0, "drawable", getPackageName());
            icon0.setImageResource(resID0);
            icon1 = findViewById(R.id.icon1);
            String iconString1 = fiveHourly.get(1).getIcon();
            int resID1 = getResources().getIdentifier(iconString1, "drawable", getPackageName());
            icon1.setImageResource(resID1);
            icon2 = findViewById(R.id.icon2);
            String iconString2 = fiveHourly.get(2).getIcon();
            int resID2 = getResources().getIdentifier(iconString2, "drawable", getPackageName());
            icon2.setImageResource(resID2);
            icon3 = findViewById(R.id.icon3);
            String iconString3 = fiveHourly.get(3).getIcon();
            int resID3 = getResources().getIdentifier(iconString3, "drawable", getPackageName());
            icon3.setImageResource(resID3);
            icon4 = findViewById(R.id.icon4);
            String iconString4 = fiveHourly.get(4).getIcon();
            int resID4 = getResources().getIdentifier(iconString4, "drawable", getPackageName());
            icon4.setImageResource(resID4);

            UIView = "current";

            toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
            {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                {
                    if (isChecked)
                    {
                        if (UIView.equals("current"))
                        {
                            //hourly temp
                            temp0.setText(fiveHourly.get(0).getTempC());
                            temp1.setText(fiveHourly.get(1).getTempC());
                            temp2.setText(fiveHourly.get(2).getTempC());
                            temp3.setText(fiveHourly.get(3).getTempC());
                            temp4.setText(fiveHourly.get(4).getTempC());

                            //no hourly low
                            tempMin0.setText("");
                            tempMin1.setText("");
                            tempMin2.setText("");
                            tempMin3.setText("");
                            tempMin4.setText("");

                            currentTemp.setText(c.getTempC());
                            cHigh.setText("Today: " + fiveForecast.get(0).getHighC());
                            cLow.setText(fiveForecast.get(0).getLowC());
                        }

                        else
                        {
                            //forecast high temp
                            temp0.setText(fiveForecast.get(1).getHighC());
                            temp1.setText(fiveForecast.get(2).getHighC());
                            temp2.setText(fiveForecast.get(3).getHighC());
                            temp3.setText(fiveForecast.get(4).getHighC());
                            temp4.setText(fiveForecast.get(5).getHighC());

                            //forecast low temp
                            tempMin0.setText(fiveForecast.get(0).getLowC());
                            tempMin1.setText(fiveForecast.get(1).getLowC());
                            tempMin2.setText(fiveForecast.get(2).getLowC());
                            tempMin3.setText(fiveForecast.get(3).getLowC());
                            tempMin4.setText(fiveForecast.get(4).getLowC());

                            currentTemp.setText(c.getTempC());
                            cHigh.setText("Today: " + fiveForecast.get(0).getHighC());
                            cLow.setText(fiveForecast.get(0).getLowC());
                        }
                    }

                    else
                    {
                        if (UIView.equals("current"))
                        {
                            //hourly temp
                            temp0.setText(fiveHourly.get(0).getTempF());
                            temp1.setText(fiveHourly.get(1).getTempF());
                            temp2.setText(fiveHourly.get(2).getTempF());
                            temp3.setText(fiveHourly.get(3).getTempF());
                            temp4.setText(fiveHourly.get(4).getTempF());

                            //no hourly low
                            tempMin0.setText("");
                            tempMin1.setText("");
                            tempMin2.setText("");
                            tempMin3.setText("");
                            tempMin4.setText("");

                            currentTemp.setText(c.getTempF());
                            cHigh.setText("Today: " + fiveForecast.get(0).getHighF());
                            cLow.setText(fiveForecast.get(0).getLowF());
                        }

                        else
                        {
                            //forecast high temp
                            temp0.setText(fiveForecast.get(1).getHighF());
                            temp1.setText(fiveForecast.get(2).getHighF());
                            temp2.setText(fiveForecast.get(3).getHighF());
                            temp3.setText(fiveForecast.get(4).getHighF());
                            temp4.setText(fiveForecast.get(5).getHighF());

                            //forecast low temp
                            tempMin0.setText(fiveForecast.get(1).getLowF());
                            tempMin1.setText(fiveForecast.get(2).getLowF());
                            tempMin2.setText(fiveForecast.get(3).getLowF());
                            tempMin3.setText(fiveForecast.get(4).getLowF());
                            tempMin4.setText(fiveForecast.get(5).getLowF());

                            currentTemp.setText(c.getTempF());
                            cHigh.setText("Today: " + fiveForecast.get(0).getHighF());
                            cLow.setText(fiveForecast.get(0).getLowF());
                        }
                    }
                }
            });
        }

    }

    public void getForecast(View v)
    {

        //current
        currentLocation.setText(c.getLocation());
        currentCondition.setText(c.getWeatherCondition());
        String imageString = c.getIcon();
        int resID = getResources().getIdentifier(imageString, "drawable", getPackageName());
        currentIcon.setImageResource(resID);

        if (toggleButton.isChecked())
        {
            //forecast high temp
            temp0.setText(fiveForecast.get(1).getHighC());
            temp1.setText(fiveForecast.get(2).getHighC());
            temp2.setText(fiveForecast.get(3).getHighC());
            temp3.setText(fiveForecast.get(4).getHighC());
            temp4.setText(fiveForecast.get(5).getHighC());

            //forecast low temp
            tempMin0.setText(fiveForecast.get(1).getLowC());
            tempMin1.setText(fiveForecast.get(2).getLowC());
            tempMin2.setText(fiveForecast.get(3).getLowC());
            tempMin3.setText(fiveForecast.get(4).getLowC());
            tempMin4.setText(fiveForecast.get(5).getLowC());

            currentTemp.setText(c.getTempC());
        }

        else
        {
            //forecast high temp
            temp0.setText(fiveForecast.get(1).getHighF());
            temp1.setText(fiveForecast.get(2).getHighF());
            temp2.setText(fiveForecast.get(3).getHighF());
            temp3.setText(fiveForecast.get(4).getHighF());
            temp4.setText(fiveForecast.get(5).getHighF());

            //forecast low temp
            tempMin0.setText(fiveForecast.get(1).getLowF());
            tempMin1.setText(fiveForecast.get(2).getLowF());
            tempMin2.setText(fiveForecast.get(3).getLowF());
            tempMin3.setText(fiveForecast.get(4).getLowF());
            tempMin4.setText(fiveForecast.get(5).getLowF());
        }

        //forecast day
        field0.setText(fiveForecast.get(1).getDay());
        field1.setText(fiveForecast.get(2).getDay());
        field2.setText(fiveForecast.get(3).getDay());
        field3.setText(fiveForecast.get(4).getDay());
        field4.setText(fiveForecast.get(5).getDay());

        //forecast icons
        String iconString0 = fiveForecast.get(1).getIcon();
        int resID0 = getResources().getIdentifier(iconString0, "drawable", getPackageName());
        icon0.setImageResource(resID0);
        String iconString1 = fiveForecast.get(2).getIcon();
        int resID1 = getResources().getIdentifier(iconString1, "drawable", getPackageName());
        icon1.setImageResource(resID1);
        String iconString2 = fiveForecast.get(3).getIcon();
        int resID2 = getResources().getIdentifier(iconString2, "drawable", getPackageName());
        icon2.setImageResource(resID2);
        String iconString3 = fiveForecast.get(4).getIcon();
        int resID3 = getResources().getIdentifier(iconString3, "drawable", getPackageName());
        icon3.setImageResource(resID3);
        String iconString4 = fiveForecast.get(5).getIcon();
        int resID4 = getResources().getIdentifier(iconString4, "drawable", getPackageName());
        icon4.setImageResource(resID4);

        UIView = "forecast";

        if (satView)
        {
            satellite.setVisibility(View.INVISIBLE);
            toggleButton.setVisibility(View.VISIBLE);
        }
    }

    public void getHourly(View v)
    {
        //current
        currentLocation.setText(c.getLocation());
        currentCondition.setText(c.getWeatherCondition());
        String imageString = c.getIcon();
        int resID = getResources().getIdentifier(imageString, "drawable", getPackageName());
        currentIcon.setImageResource(resID);

        if(toggleButton.isChecked())
        {
            //hourly temp
            temp0.setText(fiveHourly.get(0).getTempC());
            temp1.setText(fiveHourly.get(1).getTempC());
            temp2.setText(fiveHourly.get(2).getTempC());
            temp3.setText(fiveHourly.get(3).getTempC());
            temp4.setText(fiveHourly.get(4).getTempC());

            //no hourly low
            tempMin0.setText("");
            tempMin1.setText("");
            tempMin2.setText("");
            tempMin3.setText("");
            tempMin4.setText("");

            currentTemp.setText(c.getTempC());
        }

        else
        {
            //hourly temp
            temp0.setText(fiveHourly.get(0).getTempF());
            temp1.setText(fiveHourly.get(1).getTempF());
            temp2.setText(fiveHourly.get(2).getTempF());
            temp3.setText(fiveHourly.get(3).getTempF());
            temp4.setText(fiveHourly.get(4).getTempF());

            //no hourly low
            tempMin0.setText("");
            tempMin1.setText("");
            tempMin2.setText("");
            tempMin3.setText("");
            tempMin4.setText("");

            currentTemp.setText(c.getTempF());
        }

        //hourly hour
        field0.setText(fiveHourly.get(0).getHour());
        field1.setText(fiveHourly.get(1).getHour());
        field2.setText(fiveHourly.get(2).getHour());
        field3.setText(fiveHourly.get(3).getHour());
        field4.setText(fiveHourly.get(4).getHour());

        //hourly icons
        String iconString0 = fiveHourly.get(0).getIcon();
        int resID0 = getResources().getIdentifier(iconString0, "drawable", getPackageName());
        icon0.setImageResource(resID0);
        String iconString1 = fiveHourly.get(1).getIcon();
        int resID1 = getResources().getIdentifier(iconString1, "drawable", getPackageName());
        icon1.setImageResource(resID1);
        String iconString2 = fiveHourly.get(2).getIcon();
        int resID2 = getResources().getIdentifier(iconString2, "drawable", getPackageName());
        icon2.setImageResource(resID2);
        String iconString3 = fiveHourly.get(3).getIcon();
        int resID3 = getResources().getIdentifier(iconString3, "drawable", getPackageName());
        icon3.setImageResource(resID3);
        String iconString4 = fiveHourly.get(4).getIcon();
        int resID4 = getResources().getIdentifier(iconString4, "drawable", getPackageName());
        icon4.setImageResource(resID4);

        //no hourly low
        tempMin0.setText("");
        tempMin1.setText("");
        tempMin2.setText("");
        tempMin3.setText("");
        tempMin4.setText("");

        UIView = "current";

        if (satView)
        {
            satellite.setVisibility(View.INVISIBLE);
            toggleButton.setVisibility(View.VISIBLE);
        }
    }

    private class QueryCityInBackground extends AsyncTask<String, Void, CityQuery>
    {
        @Override
        protected CityQuery doInBackground(String ... tempQuery)
        {
            CityQuery cq = new CityQuery(tempQuery[0]);
            cq.fetch();

            return cq;
        }

        @Override
        protected void onPostExecute(CityQuery cq)
        {
            ArrayList<String> cities = cq.getQueryList();

            if (cities.size() == 1)
            {
                new GetWeatherInBackground().execute(tempQuery);
            }

            else if(cities.size() == 0)
            {
                Context context = getApplicationContext();
                CharSequence text = "Location not found :( \nPlease try again. ";


                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, duration);
                toast.setGravity(Gravity.TOP| Gravity.LEFT, 300, 0);
                toast.show();

                tempQuery = "autoip";
                new GetWeatherInBackground().execute(tempQuery);

            }

            else
            {
                String city = cities.get(0);
                new GetWeatherInBackground().execute(city);
            }

            input.setText(null);

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(input.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void getWeather(View v)
    {
        input = findViewById(R.id.input);
        tempQuery = input.getText().toString();

        if (tempQuery.equals(""))
        {

        }

        else
        {
            new QueryCityInBackground().execute(tempQuery);
        }
    }

    public void getSatellite(View v)
    {
        satView = true;
        satellite = findViewById(R.id.satellite);
        satellite.setVisibility(View.VISIBLE);
        toggleButton.setVisibility(View.INVISIBLE);
        satellite.loadUrl(wd.getSatelliteURL());
    }

}
