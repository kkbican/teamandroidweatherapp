package com.example.kennbican.teamandroidweatherapp;

public class Forecast
{
    String date;
    String weekday;
    String icon;
    String high_f;
    String high_c;
    String low_f;
    String low_c;
    String condition;
    String pop;

    public Forecast(String d, String wd, String i, String hf, String hc, String lf, String lc, String c, String p)
    {
        date = d;
        weekday = wd;
        icon = i;
        high_f = hf;
        high_c = hc;
        low_f = lf;
        low_c = lc;
        condition = c;
        pop = p;
    }

    public String getDate() { return date; }

    public String getDay() { return weekday; }

    public String getIcon() { return icon; }

    public String getHighF() { return high_f + "° F"; }

    public String getHighC() { return high_c + "° C"; }

    public String getLowF() { return low_f + "° F"; }

    public String getLowC() { return low_c + "° C"; }

    public String getCondition() { return condition; }

    public String getPop() { return pop + "%";}
}
