package com.example.kennbican.teamandroidweatherapp;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;


public class CityQuery
{
    String query;
    JsonElement jse;
    JsonArray jsa;

    public CityQuery ( String cq )
    {
        if (cq.contains(" "))
        {
            query = cq.replaceAll(" +", "%20");
        }

        if (cq.contains(")") || cq.contains(")"))
        {
            query = cq.replaceAll("[(]", "%28");
            query = cq.replaceAll("[)]", "%29");
        }

        else
        {
            query = cq;
        }
    }

    public void fetch()
    {
        String attemptedCityQuery = "http://autocomplete.wunderground.com/aq?query=" + query;

        try
        {
            URL wundergroundURL = new URL(attemptedCityQuery);

            InputStream is = wundergroundURL.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            jse = new JsonParser().parse(br);
        }

        catch (java.net.MalformedURLException mue)
        {
            System.out.println("URL not well formed");
            mue.printStackTrace();
        }
        catch (java.io.IOException ioe)
        {
            System.out.println("Got IO Exception");
            ioe.printStackTrace();
        }
    }

    public ArrayList<String> getQueryList()
    {
        if (jse == null) fetch();

        ArrayList<String> queryList = new ArrayList<>();

        jsa = jse.getAsJsonObject().get("RESULTS").getAsJsonArray();

        for (int i = 0; i < jsa.size(); i++)
        {
            String tempCityName = jsa.get(i).getAsJsonObject().get("name").getAsString();

            queryList.add(tempCityName);
        }

        return queryList;
    }
}
