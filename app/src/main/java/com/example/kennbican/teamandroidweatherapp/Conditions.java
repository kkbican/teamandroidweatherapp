package com.example.kennbican.teamandroidweatherapp;

public class Conditions
{
    String CurrentDate;

    String weatherCondition;
    String icon;
    String location;
    String hour;
    String tempF;
    String tempC;
    String pop;
    String windSpeed;
    String humidity;
    String windChill;
    String windChillC;
    String currentTime;

    public Conditions (String weatherCondition, String icon, String location, String hour, String tempF, String tempC, String pop, String windSpeed, String humidity, String windChill, String windChillC, String currentTime)
    {
        this.weatherCondition = weatherCondition;
        this.icon = icon;
        this.location = location;
        this.hour = hour;
        this.tempF = tempF;
        this.tempC = tempC;
        this.pop = pop;
        this.windSpeed = windSpeed;
        this.humidity = humidity;
        this.windChill = windChill;
        this.windChillC = windChillC;
        this.currentTime = currentTime;
    }
    public String getWeatherCondition()
    {
        return weatherCondition;
    }

    public String getIcon()
    {
        return icon;
    }
    public String getLocation()
    {
        return location;
    }
    public String getHour()
    {
        return hour;
    }
    public String getTempF()
    {
        return tempF+"° F";
    }
    public String getTempC()
    {
        return tempC+ "° C";
    }
    public String getPop()
    {
        return pop+"%";
    }
    public String getWindSpeed()
    {
        return windSpeed+" mph";
    }
    public String getHumidity()
    {
        return humidity;
    }
    public String getWindChill()
    {
        return windChill+"° F";
    }
    public String getWindChillC() {return windChillC+"° C";}
    public String getTime() {return currentTime;}

    public String toString()
    {
        return "Weather: "+this.getWeatherCondition()+", Icon Name: "+this.getIcon()+", Hour: "+this.getHour()+", Temp F: "+this.getTempF()+", Temp C: "+this.getTempC()+", Chance of Rain: "+this.getPop()+", Wind Speed: "+this.getWindSpeed()+", Humidity: "+this.getHumidity()+", Heat Index: "+this.getWindChill()+this.getWindChillC() + this.getTime();
    }

}
